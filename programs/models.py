from django.db import models

# Create your models here.
class Program(models.Model):
    title = models.CharField(max_length=50)
    image_link = models.URLField()
    content = models.TextField(max_length=1000)
    fundraiser = models.CharField(max_length=25)