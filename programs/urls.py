from django.conf.urls import url
from django.urls import include
from .views import index, show_details
import donation_form.urls as donation_form

app_name = 'programs'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^(?P<value>\d+)/', show_details, name='details'),
]
