from django.test import TestCase, Client
from django.urls import resolve
from .views import index, show_details
from .models import Program

# Create your tests here.
expected_title = 'Bantu Mhs Pacil Ini Keluar dari Penderitaannya'
expected_image_link = 'https://i.kym-cdn.com/photos/images/original/001/061/830/fa9.jpg'
expected_fundraiser = '#YourMentalHealthMatters'
expected_content = 'Malang sekali nasib anak ini, kebingungan mengisi isi berita, \
mahasiswa ini malah menulis Lorem ipsum dolor sit amet, consectetur \
adipiscing elit. Fusce id laoreet augue. Quisque molestie, nunc eleifend \
condimentum hendrerit, nisi ante euismod massa, in aliquam dui sapien \
et sapien. Praesent at felis vestibulum massa elementum dictum eget \
nec leo. Sed luctus elit vel tortor commodo lobortis. Suspendisse \
convallis venenatis neque, et bibendum diam volutpat vel. In hac \
habitasse platea dictumst. Nunc et consectetur leo, ac molestie orci.'


class ProgramUnitTest(TestCase):

    def setUp(self):
        Program.objects.create(
            title = expected_title,
            image_link = expected_image_link,
            content = expected_content,
            fundraiser = expected_fundraiser,
        )

    def test_program_url_is_exist(self):
        response = Client().get('/program/')
        self.assertEqual(response.status_code,200)

    def test_program_using_correct_func(self):
        found = resolve('/program/')
        self.assertEqual(found.func, index)
    
    def test_program_using_correct_template(self):
        response = Client().get('/program/')
        self.assertTemplateUsed(response, 'program_list.html')

    # Models
    def test_model_can_create_program_object(self):
        num_of_all_objects = Program.objects.all().count()
        self.assertEqual(num_of_all_objects, 1)

    # View
    def test_display_program_success(self):
        response = Client().get('/program/')
        html_response = response.content.decode('utf8')
        self.assertIn(expected_title, html_response)

    ## Program Details
    def test_program_details_url_is_exist(self):
        program = Program.objects.get(title = expected_title)
        response = Client().get('/program/'+ str(program.id) +'/')
        self.assertEqual(response.status_code,200)

    def test_program_details_using_correct_func(self):
        program = Program.objects.get(title = expected_title)
        found = resolve('/program/'+ str(program.id) +'/')
        self.assertEqual(found.func, show_details)
    
    def test_program_details_using_correct_template(self):
        program = Program.objects.get(title = expected_title)
        response = Client().get('/program/'+ str(program.id) +'/')
        self.assertTemplateUsed(response, 'program_details.html')
