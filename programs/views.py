from django.shortcuts import render
from .models import Program
from donation_form.models import Donation

# Create your views here.
response = {'n':range(6)}

def index(request):
    response['program_list'] = Program.objects.all()
    return render(request, 'program_list.html', response)

def show_details(request, value):
    response['program'] = Program.objects.get(id=value)
    program_name = response['program'].title
    print(response['program'])
    try:
        response['donation_list'] = Donation.objects.filter(program=program_name)
        total = 0
        for donation in response['donation_list']:
            total += donation.donasi
        response['total'] = total
        print(response['donation_list'])
    except:
        print('error')
        pass
    return render(request, 'program_details.html', response)