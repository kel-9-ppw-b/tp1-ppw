![build status](https://gitlab.com/kel-9-ppw-b/tp1-ppw/badges/master/build.svg)
![coverage](https://gitlab.com/kel-9-ppw-b/tp1-ppw/badges/master/coverage.svg?job=coverage)

# Tugas 1: Crowd Funding

Ini adalah *repository* Tugas 1 PPW yaitu website Crowd Funding yang dibuat oleh kelompok 9 kelas B.

## Link Heroku

https://kel-9-ppw-b.herokuapp.com/

## Anggota Kelompok

- Devin Winardi  (1706039755)
- Dhita Putri Pratama (1706039465)
- Reyhan Alhafizal (1706040082)
- Sherren Natasha (1706074650)