from django.shortcuts import render
from django.contrib.auth import logout as auth_logout
from django.http import HttpResponseRedirect

# Create your views here.
response = {}

def logout(request):
    """Logs out user"""
    auth_logout(request)
    return HttpResponseRedirect('/')

def index(request):
    return render(request, 'landing_page.html', response)