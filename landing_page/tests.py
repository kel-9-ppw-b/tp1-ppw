from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
from unittest import skip

# Create your tests here.

# Create your tests here.
class LandingPageTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing_page.html')