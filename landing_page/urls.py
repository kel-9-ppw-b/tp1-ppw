from django.urls import path
from landing_page.views import *

urlpatterns = [
    path('', index, name='landing_page'),
    path('logout', logout, name = "logout"),
]
