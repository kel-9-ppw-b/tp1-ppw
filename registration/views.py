from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth import login, authenticate
from .forms import RegistrationForm
from .models import Donatur  


# def form(request):
#     registration_form = RegistrationForm()
#     return render(request, 'registration.html', {'form': registration_form})

# def submit(request):
#     if request.method == 'POST':
#         registration_form = RegistrationForm(request.POST)
#         if registration_form.is_valid():
#             registration_form.save()
#             return HttpResponseRedirect('/program_list')
#     print(registration_form.errors)
#     return HttpResponseRedirect('/')