from django import forms
from django.views.generic import CreateView
from .models import Donatur
import datetime

class RegistrationForm(forms.ModelForm):
    class Meta:
        model = Donatur
        fields = ['Nama', 'Tanggal_Lahir', 'Email','Kata_Sandi']
        attrs={'class' : 'form-control'}
        widgets = {
            'Kata_Sandi': forms.PasswordInput(attrs=attrs),
            'Nama': forms.TextInput(attrs=attrs),
            'Email': forms.TextInput(attrs=attrs),
            'Tanggal_Lahir': forms.TextInput(attrs=attrs),
            
        }

class Signup(CreateView):
    form_class = RegistrationForm
    model = Donatur
