from django.conf.urls import url
from django.urls import include
from .views import index

app_name = 'app_profile'

urlpatterns = [
    url(r'^$', index, name='profile'),
]