$(function(){
    search($("#username").text());
	
	function search(value){
        $.ajax({
            url: "/make_donation/get_json/" + value,
            success: function(result){
                console.log(result);
                var max_items = Object.keys(result).length;
                var total_donation = 0;
                console.log(max_items);
                for (var i = 0; i < max_items; i++) {
                    var donation = result[i];
                    var program = donation.program;
                    var amount = donation.donasi;
                    total_donation += amount;
                    console.log(program);
                    console.log(amount);
                    $("#donation-list").append(
                        '<div class="donation-card col-12 px-0 py-2">\
                        <span class="program-name"><b>' + program + '</b></span>\
                        <div class="donation-amount">Rp' + amount + '</div>\
                        </div>\
                        '
                    )
                }
                $('.total-donation').html('Rp'+total_donation);
                console.log(total_donation);
            }
        })
    }
})