from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_using_correct_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)
    
    def test_profile_using_correct_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')