from django.db import models

class Donation(models.Model):
	program = models.CharField(max_length = 200)
	nama = models.CharField(max_length = 200)
	email = models.EmailField()
	donasi = models.IntegerField()
	cantumkan_nama = models.BooleanField(default=False)