from django.urls import path, re_path
from .views import make_donation, donate,get_json

app_name = 'donation_form'

urlpatterns = [
    re_path(r'^(?P<value>\d+)/', make_donation, name='make_donation'),
    re_path(r'^donate/(?P<value>\d+)/', donate, name='donate'),
    path('get_json/<str:nama>', get_json, name='get_json'),
]