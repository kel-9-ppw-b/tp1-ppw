from django import forms
from .models import Donation

class DonationForm(forms.Form):
	attrs={
		'class' : 'form-control'
	}
	attrsbox={
		'class' : 'form-check d-inline'
	}
	
	program = forms.CharField(label='Nama Program', max_length=100, widget=forms.TextInput(attrs=attrs))
	nama = forms.CharField(label='Nama Donatur', max_length=100, widget=forms.TextInput(attrs=attrs))
	email = forms.EmailField(label='Email', max_length=100, widget=forms.TextInput(attrs=attrs))
	donasi = forms.IntegerField(label='Jumlah', widget=forms.NumberInput(attrs=attrs))
	cantumkan_nama = forms.BooleanField(label="JANGAN cantumkan nama saya",required=False, widget=forms.CheckboxInput(attrs=attrsbox))