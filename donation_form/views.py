from django.shortcuts import render,redirect
from django.contrib import messages
from .models import Donation
from .forms import DonationForm
from registration.models import Donatur
from programs.models import Program
from django.http import JsonResponse, HttpResponseRedirect
import json

response = {}

def make_donation(request,value):
	form = DonationForm(request.POST or None)
	program = Program.objects.get(id=value)
	form = DonationForm()
	form.fields['program'].initial = program.title
	form.fields['nama'].initial = request.user.username
	form.fields['email'].initial = request.user.email
	response['form'] = form
	response['program'] = program
	return render(request,'make_donation.html', response)

def donate(request,value):
	form = DonationForm(request.POST)
	program = Program.objects.get(id=value)
	response['program'] = program.title
	if request.method == 'POST':
		if form.is_valid():
			cantumkan_nama = request.POST.get('cantumkan_nama','') == 'on'
			if (cantumkan_nama) :
				response['nama'] = "Anonymous"
			else :
				response['nama'] = form.cleaned_data['nama']
			nama_program = form.cleaned_data['program']
			response['email'] = form.cleaned_data['email']
			response['donasi'] = form.cleaned_data['donasi']
			response['cantumkan_nama'] = cantumkan_nama
			obj = Donation(program = response['program'], nama = response['nama'], email = response['email'], donasi = response['donasi'])
		# data = Donatur.objects.all().filter(Email = response['email']).count()
		# if (data == 0):
		# 	messages.error(request,'Email not found')
		# 	return redirect('/make_donation/'+str(value))
		# else:
			messages.error(request,'Terima kasih')
			obj.save()
			return HttpResponseRedirect('/make_donation/'+str(value)+'/')
		# return HttpResponseRedirect('/program/')
	else :
		return HttpResponseRedirect('/')


def get_json(request,nama):
	result = {}
	y = 0
	data = Donation.objects.all().filter(nama = nama)
	for i in data:
		x = {}
		x['program'] = i.program
		x['donasi'] = i.donasi
		result[y] = x
		y+=1
	return JsonResponse(result)