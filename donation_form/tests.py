from django.test import TestCase,Client
from django.urls import resolve
from .views import make_donation
from .models import Donation
from .forms import DonationForm
from programs.models import Program

class DonationFormTest(TestCase):
	def setUp(self):
		global expected_title, expected_image_link, \
		expected_fundraiser, expected_content, program

		expected_title = 'Bantu Mhs Pacil Ini Keluar dari Penderitaannya'
		expected_image_link = 'https://i.kym-cdn.com/photos/images/original/001/061/830/fa9.jpg'
		expected_fundraiser = '#YourMentalHealthMatters'
		expected_content = 'Malang sekali nasib anak ini, kebingungan mengisi isi berita...'
		program = Program(
            title = expected_title,
            image_link = expected_image_link,
            content = expected_content,
            fundraiser = expected_fundraiser,
        )
		program.save()
	
	# def test_donation_form_exist(self):
			
	# 	response = Client().get('/make_donation/'+ str(program.id)+'/')
	# 	# print('/make_donation/'+ str(program.id))
	# 	self.assertEqual(response.status_code,200)

	def test_donation_form_function(self):
		found = resolve('/make_donation/'+ str(program.id)+'/')
		self.assertEqual(found.func,make_donation)

	# def test_donation_using_make_donation_template(self):
	# 	response = Client().get('/make_donation/'+ str(program.id)+'/')
	# 	self.assertTemplateUsed(response, 'make_donation.html')

	def test_form(self):
		form_data = {'program': 'something','nama': 'something','email': 'something@gmail.com','donasi': 8000}
		form = DonationForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_model_can_create_new_donation(self):
		# global program

		new_donation = Donation.objects.create(program=program,nama ="devin", email="test@bla.com",donasi=3000)
		counting_all_available_donation = Donation.objects.all().count()
		self.assertEqual(counting_all_available_donation, 1)

	def test_form_validation_for_blank_items(self):
		form = DonationForm(data={'program': '', 'nama': '', 'email': '', 'donasi': ''})
		self.assertFalse(form.is_valid())

	# def test_donation_is_using_navbar_template(self):
	# 	response = Client().get('/make_donation' + str(program.id)+'/')
	# 	self.assertTemplateUsed(response, 'navbar.html')

	def test_form_validation_for_blank_items(self):
		form = DonationForm(data={'program':'', 'nama':'', 'email':'', 'amount':0})
		self.assertFalse(form.is_valid())






