from django.shortcuts import render
from .models import NewsEntry

# Create your views here.
response = {}

def index_news(request):
    response['news_list']= NewsEntry.objects.all().values()
    return render(request, 'news.html', response)

def show_news(request, value):
    response['news'] = NewsEntry.objects.get(id = value)
    return render(request, 'read_news.html', response)