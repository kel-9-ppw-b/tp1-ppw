from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import NewsEntry
from django.http import HttpRequest
from unittest import skip

# Create your tests here.

# Create your tests here.
class NewsTest(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_index_news_function(self):
        found = resolve('/news/')
        self.assertEqual(found.func, index_news)

    def test_using_news_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')
    
    def test_news_model(self):
        NewsEntry.objects.create(title = "title", image = "", content="")
        counter = NewsEntry.objects.all().count()
        self.assertEqual(counter,1)