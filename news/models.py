from django.db import models

# Create your models here.

class NewsEntry(models.Model):
    title = models.CharField(max_length = 399, default = "title")
    image = models.CharField(max_length = 799, default = "")
    content = models.TextField(default="")