from django.urls import path
from django.conf.urls import url
from news.views import *

urlpatterns = [
    path('', index_news, name='news'),
    url(r'^(?P<value>\d+)/$', show_news, name='show_news'),
]