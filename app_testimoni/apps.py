from django.apps import AppConfig


class AppTestimoniConfig(AppConfig):
    name = 'app_testimoni'
