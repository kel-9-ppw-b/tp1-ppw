from django.contrib import admin
from django.urls import path
from .views import about, create_post

app_name = 'app_testimoni'

urlpatterns = [
    path('', about, name='about-us'),
	path('post', create_post, name='create_post'),

]