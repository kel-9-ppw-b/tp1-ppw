# Generated by Django 2.1.1 on 2018-12-05 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('name', models.CharField(max_length=30)),
                ('email', models.EmailField(max_length=30, primary_key=True, serialize=False, unique=True)),
                ('password', models.CharField(max_length=12)),
            ],
        ),
    ]
