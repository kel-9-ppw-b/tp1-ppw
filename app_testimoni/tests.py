from django.test import TestCase, Client
from django.urls import resolve
from .views import about, create_post
from .models import Testimonial

# Create your tests here.
class CommentTest(TestCase):
    def test_about_page_url_is_exist(self):
        response = Client().get('/about-us/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_about_func(self):
        found = resolve('/about-us/')
        self.assertEqual(found.func, about)
    
    def test_model_can_create_new_comment(self):
        testimoni = Testimonial.objects.create(
            testimoni="This is a dummy comment"
        )
        counting_all_available_comments = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_comments, 1)

    def test_models_can_create_testimonial(self):
        testimoni = Testimonial.objects.create(testimoni = 'contoh testi',)
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial, 1)

    def test_post_url_is_exist(self):
        response = Client().get('/post')
        self.assertEqual(response.status_code, 404)

    
    def test_can_save_a_POST(self):
        response = self.client.post('/about-us', data = {'testimoni':'save testi',})
        counting_all_available_testimonial = Testimonial.objects.all().count()
        self.assertEqual(counting_all_available_testimonial, 0)
        self.assertEqual(response.status_code, 404)

        new_response = self.client.get('/about-us')
        html_response = new_response.content.decode('utf8')
        self.assertIn('', html_response)


    


    
