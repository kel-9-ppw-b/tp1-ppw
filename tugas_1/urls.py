"""tugas_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import landing_page.urls as landing_page
import programs.urls as programs
from donation_form.views import make_donation
import registration.urls as registration
import donation_form.urls as donation_form
import app_profile.urls as profile
import app_testimoni.urls as app_testimoni

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(landing_page)),
    path('news/', include('news.urls')),
    path('program/', include(programs)),
    path('registration/', include(registration)),
    path('make_donation/', include(donation_form)),
    path('profile/', include(profile)),
    path('about-us/', include(app_testimoni)),
]
